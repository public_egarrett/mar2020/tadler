import com.eiragarrett.tadler.business.databasemanagement.TweetDAO;
import com.eiragarrett.tadler.business.databasemanagement.TweetDAOImpl;
import com.eiragarrett.tadler.business.tweetmanagement.StoredTweetContents;
import java.io.BufferedReader;

import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Eira Garrett
 */
public class DAOTests {
    private final static String URL = "jdbc:mysql://localhost:3306/tadler?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final static String USER = "tadler";
    private final static String PASSWORD = "tadleruser";
    
    private final static Logger LOG = LoggerFactory.getLogger(DAOTests.class);
    
    
    ////////// READ TESTS ///////////
    
    /**
     * Tests retrieval of all entries.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testFindAll() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        List<StoredTweetContents> tweets = dao.findAll();
        assertEquals("testFindAll: ", 5, tweets.size());
    }
    
    /**
     * Ensures successful retrieval of a record with the given ID.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void findTweetByIDTweetExists() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents compareTo = new StoredTweetContents();
        compareTo.setStatusID(12345);
        compareTo.setHandle("steviep");
        compareTo.setName("Steve");
        compareTo.setText("This is a tweet and there's stuff in it");
        compareTo.setRetweetCount(30);
        compareTo.setFavoriteCount(5);
        
        
        StoredTweetContents tweet = dao.findByID(12345);
        
        assertEquals("findTweetByIDTweetExists: ", compareTo, tweet);
    }
    
    /**
     * Ensures that if a tweet record with the given ID does not exist, a default StoredTweetContents object is initialized.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void findTweetByIDTweetDoesNotExist() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents contents = dao.findByID(908098);
        StoredTweetContents defaulttweet = new StoredTweetContents();
        
        assertEquals("findTweetByIDTweetDoesNotExist: ", defaulttweet, contents);
    }
    
    
    /////////// CREATE TESTS ///////////
    
    /**
     * Tests adding a new tweet when no tweet with the given statusID exists.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testAddTweet() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents tweet = new StoredTweetContents("Gloria", "gloria", "Here's some text", 1, 20, 999999);
        
        int numAdded = dao.saveTweet(tweet);
        
        assertEquals("testAddTweet: ", 1, numAdded);
    }
    
    /**
     * Tests the adding of a tweet when a tweet with the given statusID already exists.
     * SQLException expected.
     * @throws SQLException
     * @throws IOException 
     */
    @Test(expected = SQLException.class)
    public void testAddTweetAlreadyExists() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents tweet = new StoredTweetContents("Steve", "steviep", "This is a tweet and there's stuff in it", 30, 5, 12345);
        
        dao.saveTweet(tweet);
    }
    
    
    /////////////// DELETE TESTS /////////////////
    
    /**
     * Tests deletion of a tweet record when that tweet record exists.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testDeleteTweetTweetExists() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        long id = 12345;
        
        int rowsAffected = dao.deleteSavedTweet(id);
        
        assertEquals("testDeleteTweetTweetExists: ", 1, rowsAffected);
    }
    
    /**
     * Tests deletion of a tweet record when that tweet record does not exist.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testDeleteTweetDoesNotExist() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        long id = 90809808;
        
        int rowsAffected= dao.deleteSavedTweet(id);
        
        assertEquals("testDeleteTweetDoesNotExist: ", 0, rowsAffected);
    }
    
    ///////////// UPDATE TESTS //////////////
    /**
     * Ensures only one row is affected when a tweet with the given statusID exists.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testUpdateTweetExists() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents tweet = new StoredTweetContents();
        tweet.setStatusID(12345);
        tweet.setName("Steve");
        tweet.setHandle("steviep");
        tweet.setText("This is still a tweet but there's new text in it now!");
        tweet.setRetweetCount(30);
        tweet.setFavoriteCount(5);
        
        int rowsAffected = dao.updateSavedTweet(tweet);
        
        assertEquals("testUpdateTweetExists: ", 1, rowsAffected);
    }
    
    /**
     * Ensures no rows are affected if an update is attempted when no matching record exists.
     * @throws SQLException
     * @throws IOException 
     */
    @Test
    public void testUpdateTweetDoesNotExist() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        StoredTweetContents tweet = new StoredTweetContents();
        tweet.setStatusID(787878);
        
        int rowsAffected = dao.updateSavedTweet(tweet);
        
        assertEquals("testUpdateTweetDoesNotExist: ", 0, rowsAffected);
    }
    
    //// TODO -- Clear DB after tests ////
    
    
    //// Prep DB before tests /////
    
    /**
     * The following several methods are based on code by Ken Fogel. See: https://gitlab.com/omniprof/jdbc_demo/blob/master/src/test/java/com/kenfogel/jdbcdemo/unittests/DemoTestCase.java
     */
    @Before
    public void seedDB() {
        LOG.info("@Before -- db seeding.");
        
        final String dbInit = loadAsString("createTweetTable.sql");
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);) {
            for (String statement : splitStatements(new StringReader(dbInit), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            LOG.error("Failed database initialization: ", e);
        }
    }

    /**
     * Loads an SQL script.
     * @param path
     * @return 
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
            Scanner scanner = new Scanner(inputStream)) {
            
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    /**
     * Extracts statements from an sql script.
     * @param reader
     * @param statementDelimiter
     * @return 
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if(line.isEmpty() || isComment(line)) {
                    continue;
                }
                
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
        } catch (IOException e) {
            LOG.error("Could not load sql: ", e);
        }
        
        return statements;
    }
    
    /**
     * Checks whether or not the given string is a comment line.
     * @param line
     * @return 
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
