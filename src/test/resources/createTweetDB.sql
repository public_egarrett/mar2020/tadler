/**
 * SQL script to build Tadler database.
 * Author:  Eira Garrett
 */

DROP DATABASE IF EXISTS TADLER;
CREATE DATABASE TADLER;

USE TADLER;

DROP USER IF EXISTS tadler@'localhost';
CREATE USER tadler@'localhost' IDENTIFIED BY 'tadleruser';
GRANT ALL ON TADLER TO tadler@'localhost';

FLUSH PRIVILEGES;