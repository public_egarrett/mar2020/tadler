DROP TABLE IF EXISTS tweets;

CREATE TABLE tweets (
    statusID BIGINT NOT NULL PRIMARY KEY,
    name VARCHAR(60) NOT NULL,
    handle VARCHAR(60) NOT NULL,
    text VARCHAR(280) NOT NULL,
    numrts int NOT NULL default 0,
    numfvs int NOT NULL default 0
);

/*Seed with test data*/
INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (12345, "Steve", "steviep", "This is a tweet and there's stuff in it", 30, 5);
INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (12346, "Karen", "karenk", "This is exactly right.", 3, 20);
INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (12347, "Georgia", "georgiah", "Proclensity", 4, 55);
INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (12348, "Eira", "eironymous", "#SaveTheOA", 6, 78);
INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (12349, "Emily", "bumlid", "I'm a goober.", 8, 4);