package com.eiragarrett.tadler.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eiragarrett.tadler.business.tweetmanagement.TweetContentsCell;
import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import com.eiragarrett.tadler.business.tweetmanagement.TimelineManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

/**
 * Manages the UI for the search pane.
 * 
 * @author Eira Garrett
 */
public class SearchFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML // fx:id="resultsView"
    private ListView<ITweetContents> resultsView; // Value injected by FXMLLoader

    @FXML // fx:id="searchField"
    private TextField searchField; // Value injected by FXMLLoader
    
    private static final Logger LOG = LoggerFactory.getLogger(SearchFXMLController.class);
    private TimelineManager manager;
    
    /**
     * Retrieves the value of the search field and uses that value to get a list of tweets matching the search terms.
     * 
     * @param event 
     */
    @FXML
    void onSearchClicked(ActionEvent event) {
        //Fetch contents of searchField
        String query = searchField.getText();
        
        if (!query.isEmpty()) {
            showResults(query);
        } else {
            //TODO - error messaging
        }
    }
    
    /**
     * Fetches tweets matching a given search query and populates the results ListView.
     * @param query 
     */
    private void showResults(String query) {
        ObservableList<ITweetContents> results = FXCollections.observableArrayList();
        resultsView.setItems(results);
        resultsView.setCellFactory(p -> new TweetContentsCell());
        
        if (manager == null) {
            manager = new TimelineManager(resultsView.getItems());
        }
        
        try {
            manager.fillSearchResults(query);
        } catch (Exception e) {
            LOG.error("Unable to display search results: ", e);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert searchBtn != null : "fx:id=\"searchBtn\" was not injected: check your FXML file 'SearchFXML.fxml'.";
        assert resultsView != null : "fx:id=\"resultsView\" was not injected: check your FXML file 'SearchFXML.fxml'.";
        assert searchField != null : "fx:id=\"searchField\" was not injected: check your FXML file 'SearchFXML.fxml'.";

    }
}
