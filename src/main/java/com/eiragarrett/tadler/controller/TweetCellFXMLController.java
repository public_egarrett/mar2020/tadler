package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.TwitterEngine;
import com.eiragarrett.tadler.business.databasemanagement.TweetDAO;
import com.eiragarrett.tadler.business.databasemanagement.TweetDAOImpl;
import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import com.eiragarrett.tadler.business.tweetmanagement.StoredTweetContents;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Manages the user interface for a single tweet.
 * @author Eira Garrett
 */
public class TweetCellFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="handletext"
    private Text handletext; // Value injected by FXMLLoader

    @FXML // fx:id="bodytext"
    private Text bodytext; // Value injected by FXMLLoader

    @FXML // fx:id="saveTweetBtn"
    private ToggleButton saveTweetBtn; // Value injected by FXMLLoader

    @FXML // fx:id="likesText"
    private Text likesText; // Value injected by FXMLLoader

    @FXML // fx:id="retweetsText"
    private Text retweetsText; // Value injected by FXMLLoader

    @FXML // fx:id="rtBtn"
    private SplitMenuButton rtBtn; // Value injected by FXMLLoader

    @FXML // fx:id="rtWithCommentBtn"
    private MenuItem rtWithCommentBtn; // Value injected by FXMLLoader

    @FXML // fx:id="rtWithoutCommentBtn"
    private MenuItem rtWithoutCommentBtn; // Value injected by FXMLLoader

    @FXML // fx:id="likeBtn"
    private Button likeBtn; // Value injected by FXMLLoader

    @FXML // fx:id="userImg"
    private ImageView userImg; // Value injected by FXMLLoader
    
    private final static Logger LOG = LoggerFactory.getLogger(TweetCellFXMLController.class);
    
    private TwitterEngine engine;
    private ITweetContents contents;
    private boolean isSaved;
    private long statusID;
    private TimelineFXMLController parentController;

    public void setParentController(TimelineFXMLController controller) {
        this.parentController = controller;
    }
    
    /**
     * Manages adding likes.
     * @param event 
     */
    @FXML
    void onLikeClicked(ActionEvent event) {
        if (engine == null) {
            engine = new TwitterEngine();
        }
        
        try {
            engine.addLike(statusID);
        } catch (TwitterException e) {
            LOG.error("Could not add like: ", e);
        }
    }
    
    /**
     * Produces a pop-up allowing the user to enter a comment to add to a retweet.
     * @param event 
     */
    private void retweetWithComment(MouseEvent event) {
         if (engine == null) {
            engine = new TwitterEngine();
        }
        
        Popup popup = new Popup();
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PopupMessagingFXML.fxml"), bundle);
            Parent popupCell = loader.load();
            PopupMessagingFXMLController controller = loader.getController();
            controller.setTimelineController(parentController);
            
            String urlString = engine.getURLString(this.statusID);
            
            controller.setUp(controller.IS_RETWEET, urlString);
            
            double x = event.getSceneX();
            double y = event.getSceneY();
            
            popup.getContent().add(popupCell);
            popupCell.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
                popup.hide();
            });
            
            Node currentNode = this.rtBtn.getParent();
            
            popup.show(currentNode, x, y);
        
        } catch (IOException | TwitterException | IllegalStateException e) {
            LOG.error("Could not generate popup: ", e);
        }
    }

    
    @FXML
    void onRetweetWithComment(ActionEvent event) {
       rtBtn.setOnMouseClicked(e -> retweetWithComment(e));
    }

    /**
     * Retweets a given tweet.
     * @param event 
     */
    @FXML
    void onRetweetWithoutComment(ActionEvent event) {
        rtBtn.setOnMouseClicked(e -> retweetWithoutComment(e));
    }
    
    /**
     * Retweets the selected tweet without adding a comment.
     * @param event 
     */
    private void retweetWithoutComment(MouseEvent event) {
            if (engine == null) {
            engine = new TwitterEngine();
        }
        
        try {
            engine.retweetStatusNoComment(statusID);
            parentController.resetTimeline();
        } catch (TwitterException e) {
            LOG.error("Could not retweet: ", e);
        }
    }

    /**
     * Saves or unsaves tweet.
     * @param event 
     */
    @FXML
    void saveTweetClicked(ActionEvent event) {
        if (isSaved) {
            //Delete tweet from database
            try {
                TweetDAO dao = new TweetDAOImpl();
                int result = dao.deleteSavedTweet(statusID);
                
                if (result > 0) {
                    isSaved = false;
                    saveTweetBtn.setSelected(isSaved);
                    saveTweetBtn.setText(resources.getString("save.button.text"));
                }
                
            } catch (SQLException | IOException e) {
                LOG.error("Could not delete tweet from database: ", e);
            }
        } else {
            //Save tweet to database
            try {
                TweetDAO dao = new TweetDAOImpl();
                
                //Generate stored tweet contents from current stored tweets
                StoredTweetContents tweet = new StoredTweetContents();
                tweet.setStatusID(this.statusID);
                tweet.setProfileImage(this.contents.getProfileImage());
                tweet.setHandle(this.contents.getHandle());
                tweet.setName(this.contents.getName());
                tweet.setText(this.contents.getText());
                tweet.setFavoriteCount(this.contents.getFavoriteCount());
                tweet.setRetweetCount(this.contents.getRetweetCount());
                
                //Attempt to store
                int saved = dao.saveTweet(tweet);
                
                if (saved > 0) {
                    isSaved = true;
                    saveTweetBtn.setSelected(isSaved);
                    saveTweetBtn.setText(resources.getString("saved.button.text"));
                }
              
            } catch (IOException | SQLException e) {
                LOG.error("Could not add tweet to database: ", e);
            }
        }
    }
    
    /**
     * Populates the current cell with the data stored in a TweetContents object.
     * @param contents
     * @param isSaved 
     */
    public void populateCell(ITweetContents contents, boolean isSaved) {
        this.statusID = contents.getStatusID();
        this.contents = contents;
        this.isSaved = isSaved;
        this.userImg.setImage(contents.getProfileImage());
        this.handletext.setText("@" + contents.getHandle());
        this.bodytext.setText(contents.getText());
        int likes = contents.getFavoriteCount();
        this.likesText.setText(resources.getString("likes.field.text") + " " + likes);
        int retweets = contents.getRetweetCount();
        this.retweetsText.setText(resources.getString("retweets.field.text") + " " + retweets);
        
        saveTweetBtn.setSelected(isSaved);
    }
    
    /**
     * Generates a popup showing the information associated with the author of a given tweet.
     * @param event 
     */
    @FXML
    void showUserInfo(MouseEvent event) {
        //Do nothing if restoring from a stored tweet
        if (contents instanceof StoredTweetContents) {
            return;
        }
        
        Popup popup = new Popup();
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/UserInfoFXML.fxml"), bundle);
            Parent popupCell = loader.load();
            UserInfoFXMLController controller = loader.getController();
            
            controller.populateByStatusID(this.statusID);
            controller.setSelf(popup);
            
            
            popup.getContent().add(popupCell);
            popup.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
                popup.hide();
            });
            
            Node currentNode = this.rtBtn.getParent();
            Bounds bounds = this.handletext.localToScene(currentNode.getLayoutBounds());
            
            double x = bounds.getCenterX();
            double y = bounds.getCenterY();
            
            popup.show(currentNode, x, y);
            
        } catch (IOException | IllegalStateException e) {
            LOG.error("Could not load userInfo popup: ", e);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert handletext != null : "fx:id=\"handletext\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert bodytext != null : "fx:id=\"bodytext\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert saveTweetBtn != null : "fx:id=\"saveTweetBtn\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert likesText != null : "fx:id=\"likesText\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert retweetsText != null : "fx:id=\"retweetsText\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert rtBtn != null : "fx:id=\"rtBtn\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert rtWithCommentBtn != null : "fx:id=\"rtWithCommentBtn\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert rtWithoutCommentBtn != null : "fx:id=\"rtWithoutCommentBtn\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert likeBtn != null : "fx:id=\"likeBtn\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";
        assert userImg != null : "fx:id=\"userImg\" was not injected: check your FXML file 'TweetCellFXML.fxml'.";

    }
}
