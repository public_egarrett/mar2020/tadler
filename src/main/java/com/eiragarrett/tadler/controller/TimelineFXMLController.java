package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.tweetmanagement.TimelineManager;
import com.eiragarrett.tadler.business.tweetmanagement.TweetContentsCell;
import com.eiragarrett.tadler.business.TwitterEngine;
import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class TimelineFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="body"
    private AnchorPane body; // Value injected by FXMLLoader

    @FXML // fx:id="sendBtn"
    private Button sendBtn; // Value injected by FXMLLoader

    @FXML // fx:id="inputBox"
    private TextArea inputBox; // Value injected by FXMLLoader

    @FXML // fx:id="timelineView"
    private ListView<ITweetContents> timelineView; // Value injected by FXMLLoader

    @FXML // fx:id="nextButton"
    private Button nextButton; // Value injected by FXMLLoader

    @FXML // fx:id="errorText"
    private Text errorText; // Value injected by FXMLLoader

    private final static Logger LOG = LoggerFactory.getLogger(TimelineFXMLController.class);
    
    private final static int CHARLIMIT = 280;
    
    private TimelineManager manager;
    private TwitterEngine engine;
    
    /**
     * A method to fetch and display the tweets from the user's home timeline. Based on code by Ken Fogel at https://gitlab.com/omniprof/tweetdemo02
     */
    public void showTimeline() {
        ObservableList<ITweetContents> timeline = FXCollections.observableArrayList();
        timelineView.setItems(timeline);
        timelineView.setCellFactory(p -> new TweetContentsCell(this));
        
        if (manager == null) {
            manager = new TimelineManager(timelineView.getItems());
        }
        
        try {
            manager.fillTimeline();
        } catch (TwitterException e) {
            LOG.error("Unable to display timeline: ", e);
            
            errorText.setText(resources.getString("timeline.fill.error"));
        }
    }
    
    /**
     * Loads another page of tweets.
     * @param event 
     */
    @FXML
    void onNextClicked(ActionEvent event) {
        try {
            manager.fillTimeline();
        } catch (TwitterException e) {
            LOG.error("Could not load timeline: ", e);
            //error messaging for user
            errorText.setText(resources.getString("timeline.fill.error"));
        }
    }
    
    /**
     * Ensures text entered in the input box cannot exceed the tweet character limit.
     * @param event 
     */
    @FXML
    void onInputChanged(KeyEvent event) {
        //TODO -- figure out a more graceful and responsive way to do this
        if (inputBox.getText().length() >= CHARLIMIT) {
            inputBox.setText(inputBox.getText().substring(0, CHARLIMIT));
        }
    }
    
    /**
     * Handles the sending of a tweet.
     * @param event 
     */
    @FXML
    void onSendClicked(ActionEvent event) {
        //Ensure message body isn't empty
        String text = inputBox.getText().trim();
        
        if (text.isEmpty()) {
            LOG.debug("Message body read as empty.");
            return;
        }
        
        //Send tweet with input box contents
        if (engine == null) {
            engine = new TwitterEngine();
        }
        
        try {
            engine.createTweet(text);
        } catch (TwitterException e) {
            LOG.error("Could not send tweet: ", e);
            
            //TODO -- error messaging for the user
            errorText.setText(resources.getString("timeline.send.error"));
        } finally {
        
            //Reset input box
            inputBox.setText("");
        
            //Refresh timeline
            resetTimeline();   
        }
    }
    
    /**
     * Used to reset the timeline after sending a tweet.
     */
    public void resetTimeline() {
        manager.resetPage();
        timelineView.refresh();
        
        try {
            manager.fillTimeline();
        } catch (TwitterException e) {
            LOG.error("Failed to load timeline: ", e);
            errorText.setText(resources.getString("timeline.fill.error"));
        }
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert body != null : "fx:id=\"body\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
        assert sendBtn != null : "fx:id=\"sendBtn\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
        assert inputBox != null : "fx:id=\"inputBox\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
        assert timelineView != null : "fx:id=\"timelineView\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
        assert nextButton != null : "fx:id=\"nextButton\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
        assert errorText != null : "fx:id=\"errorText\" was not injected: check your FXML file 'TimelineFXML.fxml'.";
    }
}
