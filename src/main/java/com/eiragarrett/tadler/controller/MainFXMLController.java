package com.eiragarrett.tadler.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles the main TADler UI.
 * @author Eira Garrett
 * @version 2019/10/26
 */
public class MainFXMLController {
    
    private final static Logger LOG = LoggerFactory.getLogger(MainFXMLController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="closeMenuItem"
    private MenuItem closeMenuItem; // Value injected by FXMLLoader

    @FXML // fx:id="aboutMenuItem"
    private MenuItem aboutMenuItem; // Value injected by FXMLLoader

    @FXML // fx:id="timelineButton"
    private Button timelineButton; // Value injected by FXMLLoader

    @FXML // fx:id="dMButton"
    private Button dMButton; // Value injected by FXMLLoader

    @FXML // fx:id="mentionsButton"
    private Button mentionsButton; // Value injected by FXMLLoader

    @FXML // fx:id="searchPaneButton"
    private Button searchPaneButton; // Value injected by FXMLLoader

    @FXML // fx:id="mainBodyPane"
    private AnchorPane mainBodyPane; // Value injected by FXMLLoader
    
    @FXML
    private Button retweetsBtn;
    
    
    @FXML // fx:id="savedTweetsBtn"
    private Button savedTweetsBtn; // Value injected by FXMLLoader

    /**
     * Handles closing the application appropriately.
     * @param event 
     */
    @FXML
    void onCloseClicked(ActionEvent event) {
        LOG.info("Exit called.");
        Platform.exit();
    }

    /**
     * Initializes and displays the Direct Message UI.
     * @param event 
     */
    @FXML
    void onDMClicked(ActionEvent event) {
        LOG.info("DM button clicked.");
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DMFXML.fxml"), bundle);
            Parent DM = loader.load();
            DMFXMLController controller = loader.getController();
            
            controller.showConversationList();
            
            //Remove existing children
            mainBodyPane.getChildren().remove(0);
                    
            //Add new child
            mainBodyPane.getChildren().add(DM);
                    
        } catch (IOException | IllegalStateException ex) {
            //Log, alert
            LOG.error("DM fxml could not be loaded: ", ex);
        } 
    }

    /**
     * Initializes and displays the Mentions UI.
     * @param event 
     */
    @FXML
    void onMentionsClicked(ActionEvent event) {
        LOG.info("Mentions button clicked.");
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MentionsFXML.fxml"), bundle);
            Parent mentions = loader.load();
            MentionsFXMLController controller = loader.getController();
            
            controller.showMentions();
            
            //Remove existing children
            mainBodyPane.getChildren().remove(0);
            
            //Add new child
            mainBodyPane.getChildren().add(mentions);
            
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Mentions fxml could not be loaded: ", ex);
        }
        
    }

    /**
     * Initializes and displays the Search UI.
     * @param event 
     */
    @FXML
    void onSearchClicked(ActionEvent event) {
        LOG.info("Search button clicked.");
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SearchFXML.fxml"), bundle);
            Parent search = loader.load();
            
            //Remove existing children
            mainBodyPane.getChildren().remove(0);
            
            //Add new child
            mainBodyPane.getChildren().add(search);
            
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Search fxml could not be loaded: ", ex);
        }
    }

    /**
     * Initializes and displays the Timeline UI.
     * @param event 
     */
    @FXML
    void onTimelineClicked(ActionEvent event) {
        LOG.info("Timeline button clicked.");
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TimelineFXML.fxml"), bundle);
            Parent timeline = loader.load();
            
            //Remove existing children
            mainBodyPane.getChildren().remove(0);
            
            //Add new child
            mainBodyPane.getChildren().add(timeline);
            
            TimelineFXMLController controller = loader.getController();
            
            controller.showTimeline();
            
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Timeline fxml could not be loaded: ", ex);
        }
    }
    
    
    
    /**
     * Ensures the user's home timeline will be displayed upon program launch.
     */
    public void loadTimelineOnLaunch() {
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TimelineFXML.fxml"), bundle);
            Parent timeline = loader.load();
            TimelineFXMLController controller = loader.getController();
            
            controller.showTimeline();
            
            //Add new child
            mainBodyPane.getChildren().add(timeline);
            
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Timeline fxml could not be loaded: ", ex);
        }
    }
    
    /**
     * Loads the retweets UI.
     * @param event 
     */
    @FXML
    void onRetweetsClicked(ActionEvent event) {
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/RetweetsFXML.fxml"), bundle);
            Parent retweets = loader.load();
            RetweetsFXMLController controller = loader.getController();
            
            controller.loadMyRetweets();
            
            mainBodyPane.getChildren().remove(0);
            mainBodyPane.getChildren().add(retweets);
            
        } catch (IOException | IllegalStateException e) {
            LOG.error("Could not load retweets pane: ", e);
        }
    }
    
    /**
     * Loads the saved tweets UI.
     * @param event 
     */
    @FXML
    void onSavedTweetsClicked(ActionEvent event) {
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SavedTweetsFXML.fxml"), bundle);
            Parent savedTweets = loader.load();
            SavedTweetsFXMLController controller = loader.getController();
            
            controller.showSavedTweets();
            
            mainBodyPane.getChildren().remove(0);
            mainBodyPane.getChildren().add(savedTweets);
            
        } catch (IOException | IllegalStateException e) {
            LOG.error("Could not load savedTweets pane: ", e);
        }
    }
    
    
    @FXML
    void onAboutSelected(ActionEvent event) {
        WebView webView = new WebView();
        WebEngine engine = webView.getEngine();
        String pathString = MainFXMLController.class.getClassLoader().getResource("help/helphtml.html").toString();
        engine.load(pathString);
        
        webView.setMaxWidth(mainBodyPane.getWidth());
        webView.setMaxHeight(mainBodyPane.getHeight());
        
        mainBodyPane.getChildren().remove(0);
        mainBodyPane.getChildren().add(webView);
    }
            
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert closeMenuItem != null : "fx:id=\"closeMenuItem\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert aboutMenuItem != null : "fx:id=\"aboutMenuItem\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert timelineButton != null : "fx:id=\"timelineButton\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert dMButton != null : "fx:id=\"dMButton\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert mentionsButton != null : "fx:id=\"mentionsButton\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert searchPaneButton != null : "fx:id=\"searchPaneButton\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert retweetsBtn != null : "fx:id=\"retweetsBtn\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert savedTweetsBtn != null : "fx:id=\"savedTweetsBtn\" was not injected: check your FXML file 'MainFXML.fxml'.";
        assert mainBodyPane != null : "fx:id=\"mainBodyPane\" was not injected: check your FXML file 'MainFXML.fxml'.";
    }
}
