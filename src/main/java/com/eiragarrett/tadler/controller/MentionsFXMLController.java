package com.eiragarrett.tadler.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eiragarrett.tadler.business.tweetmanagement.TimelineManager;
import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import com.eiragarrett.tadler.business.tweetmanagement.TweetContentsCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Handles the UI for viewing tweets in which the current user was mentioned.
 * @author Eira Garrett
 */
public class MentionsFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mentionsView"
    private ListView<ITweetContents> mentionsView; // Value injected by FXMLLoader
    
    private static final Logger LOG = LoggerFactory.getLogger(MentionsFXMLController.class);
    private TimelineManager manager;
    
    /**
     * Fetches the list of tweets in which the user was mentioned and uses this list to populate the ListView.
     */
    public void showMentions() {
        ObservableList<ITweetContents> mentions = FXCollections.observableArrayList();
        mentionsView.setItems(mentions);
        mentionsView.setCellFactory(p -> new TweetContentsCell());
        
        if (manager == null) {
            manager = new TimelineManager(mentionsView.getItems());
        }
        
        try {
            manager.fillMentions();
        } catch (Exception e) {
            LOG.error("Unable to display mentions: ", e);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert mentionsView != null : "fx:id=\"mentionsView\" was not injected: check your FXML file 'MentionsFXML.fxml'.";

    }
}