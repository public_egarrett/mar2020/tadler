package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.TwitterEngine;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Manages the user interface for sending a direct message/retweet from the timeline UI. Which version will be instantiated can be selected using the setUp method.
 * @author Eira Garrett
 */
public class PopupMessagingFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="msgInput"
    private TextArea msgInput; // Value injected by FXMLLoader

    @FXML // fx:id="sendBtn"
    private Button sendBtn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectText"
    private Text subjectText; // Value injected by FXMLLoader
    
    private final static Logger LOG = LoggerFactory.getLogger(PopupMessagingFXMLController.class);
    
    public final Boolean IS_RETWEET = true;
    public final Boolean IS_NOT_RETWEET = false;
    
    private String target;
    private String retweetUrl;
    private boolean isRetweet;
    private TwitterEngine engine;
    private TimelineFXMLController timelineController;

    public void setTimelineController(TimelineFXMLController controller) {
        this.timelineController = controller;
    }
    
    /**
     * Sends either the retweet or direct message, depending on which version of the pane was instantiated.
     * @param event 
     */
    @FXML
    void sendClicked(ActionEvent event) {
        if (engine == null) {
            engine = new TwitterEngine();
        }
        
        try {
            if (isRetweet) {
                String text = compRetweet();
            
                engine.createTweet(text);
                
                msgInput.setText("");
                
                if (timelineController != null) {
                    timelineController.resetTimeline();
                }
            } else {
                String text = msgInput.getText();
                
                engine.sendDirectMessage(target, text);
                
                msgInput.setText("");
            }
        } catch (TwitterException e) {
            LOG.error("Could not send tweet or DM: ", e);
        }
    }
    
    /**
     * Sets up the UI.
     * @param isRetweet
     * @param target 
     */
    public void setUp(boolean isRetweet, String target) {
        if (isRetweet) {
            this.subjectText.setText(resources.getString("retweet.target.text") + target);
            this.retweetUrl = target;
        } else {
            this.subjectText.setText(resources.getString("newdm.target.text") + target);
            this.target = target;
        }
        
        this.isRetweet = isRetweet;
        this.engine = new TwitterEngine();
    }
    
    /**
     * Composes a url to include as a quote in a retweet.
     * @return 
     */
    private String compRetweet() {
        String contents = msgInput.getText();
        contents = contents.trim();
        
        if (contents.length() > 280) {
            contents = contents.substring(0, 280);
        }
        
        contents = contents.concat(" ").concat(retweetUrl);
        
        return contents;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert msgInput != null : "fx:id=\"msgInput\" was not injected: check your FXML file 'PopupMessagingFXML.fxml'.";
        assert sendBtn != null : "fx:id=\"sendBtn\" was not injected: check your FXML file 'PopupMessagingFXML.fxml'.";
        assert subjectText != null : "fx:id=\"subjectText\" was not injected: check your FXML file 'PopupMessagingFXML.fxml'.";

    }
}
