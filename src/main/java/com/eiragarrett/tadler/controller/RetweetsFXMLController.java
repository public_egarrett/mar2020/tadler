package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.tweetmanagement.TimelineManager;
import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import com.eiragarrett.tadler.business.tweetmanagement.TweetContentsCell;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Manages the UI for the retweets tab.
 * @author Eira Garrett
 */
public class RetweetsFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="retweetsView"
    private ListView<ITweetContents> retweetsView; // Value injected by FXMLLoader

    @FXML // fx:id="byMeBtn"
    private Button byMeBtn; // Value injected by FXMLLoader

    @FXML // fx:id="byOthersBtn"
    private Button byOthersBtn; // Value injected by FXMLLoader
    
    @FXML // fx:id="nextButton"
    private Button nextButton; // Value injected by FXMLLoader

    
    //Enum to store which timeline is selected
    private enum selected {
        MINE, THEIRS;
    }
    
    private static final Logger LOG = LoggerFactory.getLogger(RetweetsFXMLController.class);
    private TimelineManager manager;
    private selected isSelected = selected.MINE;
    
   
    /**
     * Switches the ListView to display a list of tweets retweeted by the current user.
     * @param event 
     */
    @FXML
    void byMeClicked(ActionEvent event) {
        LOG.debug("Retweets By Me clicked.");
        
        isSelected = selected.MINE;
        
        loadMyRetweets();
    }

    /**
     * Switches the ListView to display a list of tweets made by the current user and retweeted by others.
     * @param event 
     */
    @FXML
    void byOthersClicked(ActionEvent event) {
        isSelected = selected.THEIRS;
        
        ObservableList<ITweetContents> timeline = FXCollections.observableArrayList();
        
        retweetsView.setItems(timeline);
        retweetsView.setCellFactory(p -> new TweetContentsCell());
        
        if (manager == null) {
            manager = new TimelineManager(retweetsView.getItems());
        }
        
        manager.resetPage();
        
        try {
            manager.fillRetweetsByOthers();
        } catch (TwitterException e) {
            LOG.error("Could not load retweets by others: ", e);
            
            //TODO -- helpful error messaging
        }
    }
    
    /**
     * Loads a list of tweets retweeted by the current user.
     */
    public void loadMyRetweets() {
        ObservableList<ITweetContents> timeline = FXCollections.observableArrayList();
        
        retweetsView.setItems(timeline);
        retweetsView.setCellFactory(p -> new TweetContentsCell());
        
        if (manager == null) {
            manager = new TimelineManager(retweetsView.getItems());
        }
        
        //Reset in case timeline already filled
        manager.resetPage();
        
        try {
            manager.fillRetweetsByMe();
        } catch (TwitterException e) {
            LOG.error("Could not load retweets by me: ", e);
            //TODO -- error messaging
        }
    }
    
    /**
     * Loads the next 20 tweets, depending on which timeline is currently being shown.
     * @param event 
     */
    @FXML
    void onNextButtonClicked(ActionEvent event) {
        try {
            if (isSelected == selected.MINE) {
                manager.fillRetweetsByMe();
            } else if (isSelected == selected.THEIRS) {
                manager.fillRetweetsByOthers();
            }
        } catch (TwitterException e) {
            LOG.error("Could not load more tweets: ", e);
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert retweetsView != null : "fx:id=\"retweetsView\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";
        assert byMeBtn != null : "fx:id=\"byMeBtn\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";
        assert byOthersBtn != null : "fx:id=\"byOthersBtn\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";
        assert nextButton != null : "fx:id=\"nextButton\" was not injected: check your FXML file 'RetweetsFXML.fxml'.";

    }
}
