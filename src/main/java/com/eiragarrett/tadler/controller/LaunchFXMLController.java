package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.PropertiesBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import com.eiragarrett.tadler.business.PropertiesManager;
import com.mysql.cj.util.StringUtils;
import java.io.IOException;
import java.util.Locale;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles the form UI.
 * @author Eira Garrett
 * @version 2019/10/06
 */
public class LaunchFXMLController {
    //Logger
    private final static Logger LOG = LoggerFactory.getLogger(LaunchFXMLController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="consumerField"
    private TextField consumerField; // Value injected by FXMLLoader

    @FXML // fx:id="consumerSecretField"
    private TextField consumerSecretField; // Value injected by FXMLLoader

    @FXML // fx:id="accessField"
    private TextField accessField; // Value injected by FXMLLoader

    @FXML // fx:id="accessSecretField"
    private TextField accessSecretField; // Value injected by FXMLLoader

    @FXML // fx:id="continueBtn"
    private Button continueBtn; // Value injected by FXMLLoader

    /**
     * Handles the retrieval and processing of user-defined information. Ensures values have been entered,
     * then generates a properties file based on those values and attempts to instantiate Twitter object and 
     * proceed to main application.
     * @param event 
     */
    @FXML
    void onContinueClicked(ActionEvent event) {
        //Retrieve TextField values
        String consumer = consumerField.getText().trim();
        String consumerSecret = consumerSecretField.getText().trim();
        String access = accessField.getText().trim();
        String accessSecret = accessSecretField.getText().trim();
        
        //Ensure TextFields have values -- TODO this is ugly, see if you can't fix that
        if (StringUtils.isEmptyOrWhitespaceOnly(consumer) || StringUtils.isEmptyOrWhitespaceOnly(consumerSecret) || StringUtils.isEmptyOrWhitespaceOnly(access) || StringUtils.isEmptyOrWhitespaceOnly(accessSecret)) {
            //TODO - Alert time
        } else {
            //Pass values to PropertiesManager
            PropertiesManager instantiator = new PropertiesManager();
            try {
                //Generate properties bean
                PropertiesBean bean = new PropertiesBean();
                
                bean.setConsumerKey(consumer);
                bean.setConsumerSecret(consumerSecret);
                bean.setAccessToken(access);
                bean.setAccessTokenSecret(accessSecret);
                
                instantiator.generateProperties(bean);
                
                //TODO - instantiate twitter object before proceeding
                
                //Proceed to Main activity
                try {
                    Locale locale = new Locale("en_CA");
                    ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
                    
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainFXML.fxml"), bundle);
                    Parent root = loader.load();
                    MainFXMLController controller = loader.getController();
                    controller.loadTimelineOnLaunch();
                    
                    continueBtn.getScene().setRoot(root);
                    
                } catch (IOException | IllegalStateException ex) {
                  //Log, alert
                  LOG.error("Main scene could not be loaded: ", ex);
                } 
            } catch (IOException e) {
                //TODO - Alert, log
                LOG.error("Could not generate properties file: ", e);
            }
        }
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert consumerField != null : "fx:id=\"consumerField\" was not injected: check your FXML file 'LaunchFXML.fxml'.";
        assert consumerSecretField != null : "fx:id=\"consumerSecretField\" was not injected: check your FXML file 'LaunchFXML.fxml'.";
        assert accessField != null : "fx:id=\"accessField\" was not injected: check your FXML file 'LaunchFXML.fxml'.";
        assert accessSecretField != null : "fx:id=\"accessSecretField\" was not injected: check your FXML file 'LaunchFXML.fxml'.";
        assert continueBtn != null : "fx:id=\"continueBtn\" was not injected: check your FXML file 'LaunchFXML.fxml'.";

    }
}
