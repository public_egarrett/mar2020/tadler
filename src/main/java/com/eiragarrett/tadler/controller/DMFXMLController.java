package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.directmessagemanagement.DMContents;
import com.eiragarrett.tadler.business.directmessagemanagement.DMContentsListCell;
import com.eiragarrett.tadler.business.directmessagemanagement.DMConversationListCell;
import com.eiragarrett.tadler.business.directmessagemanagement.DMManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eiragarrett.tadler.business.TwitterEngine;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import twitter4j.TwitterException;

/**
 * Handles the UI for viewing and interacting with direct message conversations.
 * 
 * @author Eira Garrett
 * @version 2019/10/26
 */
public class DMFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="userList"
    private ListView<DMContents> userList; // Value injected by FXMLLoader

    @FXML // fx:id="sendBtn"
    private Button sendBtn; // Value injected by FXMLLoader

    @FXML // fx:id="inputBox"
    private TextArea inputBox; // Value injected by FXMLLoader

    @FXML // fx:id="conversationList"
    private ListView<DMContents> conversationList; // Value injected by FXMLLoader
    
    @FXML // fx:id="userField"
    private TextField userField; // Value injected by FXMLLoader

    @FXML // fx:id="newText"
    private Text newText; // Value injected by FXMLLoader

    @FXML // fx:id="newCheckBox"
    private CheckBox newCheckBox; // Value injected by FXMLLoader

    @FXML // fx:id="errorText"
    private Text errorText; // Value injected by FXMLLoader

    @FXML // fx:id="usernameLabel"
    private Text usernameLabel; // Value injected by FXMLLoader

    
    private static final Logger LOG = LoggerFactory.getLogger(DMFXMLController.class);
    private DMManager conversationListManager;
    private DMManager conversationManager;
    
    private long currentUserID;
    private long conversationPartnerID;
    
    private static final int CHARLIMIT = 100000;

    /**
     * Ensures text entered in the input box cannot exceed the direct message character limit.
     * @param event 
     */
    @FXML
    void onInputChange(KeyEvent event) {
        if (inputBox.getText().length() >= CHARLIMIT) {
            inputBox.setText(inputBox.getText().substring(0, CHARLIMIT));
        }
    }

    /**
     * Sends a direct message, either to the current conversation or to the specified user.
     * @param event 
     */
    @FXML
    void onSendClicked(ActionEvent event) {
        String message = inputBox.getText();
        
        TwitterEngine engine = new TwitterEngine();
        
        //Ensure message box is not empty.
        if (message.trim().isEmpty()) {
            LOG.debug("That there message is empty, goofball.");
            //display error messaging
            errorText.setText(resources.getString("dm.message.empty"));
            return;
        }
        
        //send DM
        if (newCheckBox.selectedProperty().getValue()) {
            String userName = userField.getText();
            
            if (userName.trim().isEmpty()) {
                LOG.debug("Username field is empty.");
                //display error messaging
                errorText.setText(resources.getString("dm.username.empty"));
                return;
            } else {
                try {
                    engine.sendDirectMessage(userName, message.trim());
                } catch (TwitterException e) {
                    LOG.error("Direct message could not be sent: ", e);
                    
                    //error messaging
                    errorText.setText(resources.getString("dm.send.failure"));
                }
            }
        } else {
            //Ensure a conversation is selected
            if (conversationPartnerID == 0L) {
                LOG.debug("No conversation selected.");
                
                //messaging
                errorText.setText(resources.getString("dm.noconvo.error"));
                return;
            }
            
            try {
                engine.sendDirectMessage(conversationPartnerID, message.trim());
            } catch (TwitterException e) {
                LOG.error("Could not send direct message: ", e);
                
                //error messaging
                errorText.setText(resources.getString("dm.send.failure"));
            }
        }
        
        //Reset input
        inputBox.setText("");
        
        
        //Reload conversation, if in one already
        if (!(newCheckBox.selectedProperty().getValue())) {
            reloadCurrentConversation();
        }
        
        //Reload conversation list
        reloadConversationList();
    }
    
    /**
     * Reloads the conversation currently being displayed.
     */
    private void reloadCurrentConversation() {
        this.conversationManager.resetPage();
        this.conversationList.refresh();
        
        try {
            this.conversationManager.fillSingleConvo(conversationPartnerID);
        } catch (TwitterException e) {
            LOG.error("Could not reload current conversation: ", e);
            errorText.setText(resources.getString("dm.load.failure"));
        }
    }
    
    /**
     * Reloads the list of conversations available to the user.
     */
    private void reloadConversationList() {
        this.conversationListManager.resetPage();
        this.userList.refresh();
        
        try {
            this.conversationListManager.fillConversationList();
        } catch (TwitterException e) {
            LOG.error("Could not reload conversation list: ", e);
            
            //TODO -- give this its own error string
            errorText.setText(resources.getString("dm.load.failure"));
        }
    }
    
    /**
     * Toggles UI elements depending on whether or not the user wishes to send a new DM.
     * If so, a box in which they can enter a username will be displayed.
     * @param event 
     */
    @FXML
    void onCheckClicked(ActionEvent event) {
        if (newCheckBox.selectedProperty().getValue()) {
            userField.setVisible(true);
            usernameLabel.setVisible(true);
        } else {
            userField.setVisible(false);
            usernameLabel.setVisible(false);
        }
    }
    
    /**
     * Populates the list of conversations.
     */
    public void showConversationList() {
        LOG.debug("Reached DMFXMLController.showConversationList.");
        
        ObservableList<DMContents> convoList = FXCollections.observableArrayList();
        
        userList.setItems(convoList);
        userList.setCellFactory(p -> new DMConversationListCell());
        
        userList.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends DMContents> ov, DMContents t, DMContents t1) -> {
            if (t1 != null) {
                try {
                    LOG.debug("New handle: ", t1.getSendingUserHandle());
                    
                    setConversationalPartner(t1.getSendingUser(), t1.getReceivingUser());
                    
                    //populate conversation list
                    getSelectedConversation();
                } catch (TwitterException e) {
                    LOG.error("Oopsies: ", e);
                }
            }
        });
        
        //Set the current user's ID
        TwitterEngine engine = new TwitterEngine();
        try {
            currentUserID = engine.getCurrentUserID();
        } catch (TwitterException e) {
            LOG.error("Unable to get current user ID: ", e);
        }
        
        if (conversationListManager == null) {
            conversationListManager = new DMManager(userList.getItems());
        }
        
        try {
            conversationListManager.fillConversationList();
        } catch (TwitterException e) {
            LOG.error("Unable to display DM list: ", e);
        }
    }
    
    /**
     * Checks the IDs of both conversational partners against the current user's ID, sets the other as the ID of the conversational
     * partner for later use when sending messages.
     * @param sender
     * @param receiver 
     */
    private void setConversationalPartner(long sender, long receiver) {
        LOG.debug("Reached setConversationalPartner, ", sender, receiver);
        
        if (sender == currentUserID) {
            conversationPartnerID = receiver;
        } else if (receiver == currentUserID) {
            conversationPartnerID = sender;
        }
    }
    
    /**
     * Populates the list of messages in the current conversation based on the selected value in the list.
     */
    private void getSelectedConversation() {
        LOG.debug("Reached getSelectedConversation.");
        
        ObservableList<DMContents> list = FXCollections.observableArrayList();
        conversationList.setItems(list);
        conversationList.setCellFactory(p -> new DMContentsListCell());
        
        if (conversationManager == null) {
            conversationManager = new DMManager(conversationList.getItems());
        }
        
        try {
            conversationManager.fillSingleConvo(conversationPartnerID);
        } catch (TwitterException e) {
            LOG.error("Could not load conversation: ", e);
            
            //error messaging for user
            errorText.setText(resources.getString("dm.load.failure"));
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert userList != null : "fx:id=\"userList\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert sendBtn != null : "fx:id=\"sendBtn\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert inputBox != null : "fx:id=\"inputBox\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert conversationList != null : "fx:id=\"conversationList\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert userField != null : "fx:id=\"userField\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert newText != null : "fx:id=\"newText\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert newCheckBox != null : "fx:id=\"newCheckBox\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert errorText != null : "fx:id=\"errorText\" was not injected: check your FXML file 'DMFXML.fxml'.";
        assert usernameLabel != null : "fx:id=\"usernameLabel\" was not injected: check your FXML file 'DMFXML.fxml'.";
    }
}