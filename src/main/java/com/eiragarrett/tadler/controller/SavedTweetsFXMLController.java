package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.tweetmanagement.ITweetContents;
import com.eiragarrett.tadler.business.tweetmanagement.TimelineManager;
import com.eiragarrett.tadler.business.tweetmanagement.TweetContentsCell;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SavedTweetsFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="savedTweetsView"
    private ListView<ITweetContents> savedTweetsView; // Value injected by FXMLLoader
    
    private static final Logger LOG = LoggerFactory.getLogger(SavedTweetsFXMLController.class);
    private TimelineManager manager;

    public void showSavedTweets() {
        ObservableList<ITweetContents> savedTweets = FXCollections.observableArrayList();
        savedTweetsView.setItems(savedTweets);
        savedTweetsView.setCellFactory(p-> new TweetContentsCell());
        
        if (manager == null) {
            manager = new TimelineManager(savedTweetsView.getItems());
        }
        
        try {
            manager.fillSavedTweets();
        } catch (SQLException e) {
            LOG.error("Error retrieving tweets from database: ", e);
        } catch (Exception e) {
            LOG.error("Unable to display saved tweets: ", e);
        }
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert savedTweetsView != null : "fx:id=\"savedTweetsView\" was not injected: check your FXML file 'SavedTweetsFXML.fxml'.";

    }
}