/**
 * Sample Skeleton for 'UserInfoFXML.fxml' Controller Class
 */

package com.eiragarrett.tadler.controller;

import com.eiragarrett.tadler.business.TwitterEngine;
import com.eiragarrett.tadler.business.tweetmanagement.AuthorInfoManager;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * Displays information on a given user and provides functionality for following/unfollowing or DMing that user.
 * @author Eira Garrett
 */
public class UserInfoFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="followBtn"
    private Button followBtn; // Value injected by FXMLLoader

    @FXML // fx:id="handleTxt"
    private Text handleTxt; // Value injected by FXMLLoader

    @FXML // fx:id="dmButton"
    private Button dmButton; // Value injected by FXMLLoader

    @FXML // fx:id="nameTxt"
    private Text nameTxt; // Value injected by FXMLLoader

    @FXML // fx:id="followersText"
    private Text followersText; // Value injected by FXMLLoader

    @FXML // fx:id="followingTxt"
    private Text followingTxt; // Value injected by FXMLLoader

    @FXML // fx:id="descriptionText"
    private Text descriptionText; // Value injected by FXMLLoader
    
    @FXML // fx:id="closeBtn"
    private Button closeBtn; // Value injected by FXMLLoader
    
    private static final Logger LOG = LoggerFactory.getLogger(UserInfoFXMLController.class);
    private long statusID;
    private long userID;
    private Boolean isFollowing = false;
    private TwitterEngine engine;
    private AuthorInfoManager manager;
    private Popup self;
    
    /**
     * Gives the controller a handle to the popup.
     * @param self 
     */
    public void setSelf(Popup self) {
        this.self = self;
    }
    
    /**
     * Closes the current popup.
     * @param event 
     */
    @FXML
    void closeButtonClicked(ActionEvent event) {
        if (self != null) {
            self.hide();
        }
    }

    /**
     * Displays another popup with a field for entering content to begin a new DM conversation.
     * @param event 
     */
    @FXML
    void onDMClicked(MouseEvent event) {
        Popup popup = new Popup();
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PopupMessagingFXML.fxml"), bundle);
            Parent popupCell = loader.load();
            PopupMessagingFXMLController controller = loader.getController();
            
            controller.setUp(controller.IS_NOT_RETWEET, manager.getHandle());
            
            popup.getContent().add(popupCell);
            popupCell.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
                popup.hide();
            });
           
            
            Node currentNode = this.dmButton.getParent();
            Bounds bounds = this.dmButton.localToScene(currentNode.getLayoutBounds());
            
            double x = bounds.getCenterX();
            double y = bounds.getCenterY();
            
            popup.show(currentNode, x, y);
            
        } catch (IOException | IllegalStateException e) {
            LOG.error("Could not display message pane: ", e);
        }
    }
    

    /**
     * Attempts to follow the given user and updates UI accordingly.
     * @param event 
     */
    @FXML
    void onFollowClicked(ActionEvent event) {
        if (this.engine == null) {
            this.engine = new TwitterEngine();
        }
        
        try {
            if (this.isFollowing) {
                engine.unfollowAuthor(userID);
                updateIfFollowing();
                updateFollowButton();
            } else {
                engine.followAuthor(userID);
                updateIfFollowing();
                updateFollowButton();
            }
        } catch (TwitterException e) {
            LOG.error("Could not follow/unfollow author: ", e);
        }
    }
    
    /**
     * Populates the cell with information about the author of the current status.
     * @param statusID 
     */
    public void populateByStatusID(long statusID) {
        this.statusID = statusID;
        try {
            this.manager = new AuthorInfoManager(statusID);
        } catch (TwitterException e) {
            LOG.error("Could not instantiate AuthorInfoManager: ", e);
        }
        
        updateIfFollowing();
        updateFollowButton();
        
        this.userID = manager.getID();
        this.handleTxt.setText("@" + manager.getHandle());
        this.nameTxt.setText(manager.getName());
        this.descriptionText.setText(manager.getProfileInfo());
        this.descriptionText.setWrappingWidth(240);
        this.followersText.setText(resources.getString("userinfo.follower.text") + manager.getFollowerCount());
        this.followingTxt.setText(resources.getString("userinfo.following.text") + manager.getFollowingCount());
        
        setUpDMButton();
    }
    
    /**
     * Checks whether or not the user in question is followed by the currently authenticated user and updates the UI accordingly.
     */
    private void updateIfFollowing() {
        if (this.manager != null) {
            try {
                this.isFollowing = manager.isFollowedByMe();
            } catch (TwitterException e) {
                LOG.error("Could not update whether or not user is following author: ", e);
            }
        }
    }
    
    /**
     * Updates the UI based on whether or not the current user is following the user represented by the current cell.
     */
    private void updateFollowButton() {
        if (this.isFollowing == true) {
            this.followBtn.setText(resources.getString("unfollow.button.text"));
        } else {
            this.followBtn.setText(resources.getString("follow.button.text"));
        }
    }
    
    /**
     * Shows or hides the DM Button based on whether or not the current user is followed by the author of the given status.
     */
    private void setUpDMButton() {
        try {
            if (manager == null) {
                manager = new AuthorInfoManager(statusID);
            }
            
            if (manager.isFollowingMe()) {
                this.dmButton.setVisible(true);
            } else {
                this.dmButton.setVisible(false);
            }
        } catch (TwitterException e) {
            LOG.error("Could not update DM buttons: ", e);
        }
    }
    

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert followBtn != null : "fx:id=\"followBtn\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert handleTxt != null : "fx:id=\"handleTxt\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert dmButton != null : "fx:id=\"dmButton\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert nameTxt != null : "fx:id=\"nameTxt\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert followersText != null : "fx:id=\"followersText\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert followingTxt != null : "fx:id=\"followingTxt\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert descriptionText != null : "fx:id=\"createdText\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
        assert closeBtn != null : "fx:id=\"closeBtn\" was not injected: check your FXML file 'UserInfoFXML.fxml'.";
    }
}
