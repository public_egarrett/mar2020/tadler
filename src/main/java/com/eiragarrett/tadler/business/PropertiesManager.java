package com.eiragarrett.tadler.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;

import java.util.Properties;


/**
 * Based on code by Ken Fogel (https://gitlab.com/omniprof/tweetdemo01/)
 * Handles instantiation of Twitter object singleton, as well as generation of
 * properties file if necessary.
 * 
 * @author Eira Garrett
 * @version 2019/10/06
 */
public class PropertiesManager {
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);
    private final File propFile;
    
    /**
     * Initializes path to properties file.
     */
    public PropertiesManager() {
        this.propFile = new File("/main/resources/twitter4j.properties");
    }
    
    /**
     * Instantiates a Twitter singleton to be used throughout application.
     * @return                     A Twitter object generated based on data contained in the twitter4j.properties file.
     */
    public Twitter getTwitterInstance() {
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;
    }
    
    /**
     * Generates a twitter4j.properties file if none exists.
     * @param properties
     * @throws IOException 
     */
    public void generateProperties(PropertiesBean properties) throws IOException {
        //Generate and set properties.
        Properties props = new Properties();
        props.setProperty("oauth.consumerKey", properties.getConsumerKey());
        props.setProperty("oauth.consumerSecret", properties.getConsumerSecret());
        props.setProperty("oauth.accessToken", properties.getAccessToken());
        props.setProperty("oauth.accessTokenSecret", properties.getAccessTokenSecret());
        
        //You really shouldn't be getting here if file exists, but just in case:
         if(!propFile.exists()) {
             propFile.createNewFile();
             
             //Store properties in file
             saveProperties(props);
         } else {
             //Log
             LOG.debug("Somehow you've managed to reach generateProperties despite the fact that a properties file already exists.");
         }
   }
    
    /**
     * Writes properties to properties file.
     * @param p               A Properties object containing the relevant data retrieved from the user.
     * @throws IOException 
     */
    private void saveProperties(Properties p) throws IOException {
        //Store properties
        try (
            //Open stream
            FileOutputStream outStream = new FileOutputStream(propFile)) {
            //Store properties
            p.store(outStream, "Properties");
        }
    }
    
}
