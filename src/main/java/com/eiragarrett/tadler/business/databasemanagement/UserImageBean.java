package com.eiragarrett.tadler.business.databasemanagement;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Eira Garrett
 */
public class UserImageBean {
    private String handle;
    private String url;
    private Image image;
    private byte[] blob;
    
    public UserImageBean() {
        super();
        handle = "";
        url = "";
        blob = new byte[0];
    }
    
    public UserImageBean(final String handle, final String url) throws MalformedURLException, IOException {
        this.handle = handle;
        this.url = url;
        this.image = new Image(url, 48, 48, true, false);
        
        URL imgUrl = new URL(url);
        BufferedImage bufferedImage = ImageIO.read(imgUrl);
        
    }
    
    public void setHandle(String handle) {
        this.handle = handle;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public void setImage(Image image) {
        this.image = image;
    }
    
    
}
