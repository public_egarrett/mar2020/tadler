package com.eiragarrett.tadler.business.databasemanagement;

import com.eiragarrett.tadler.business.tweetmanagement.StoredTweetContents;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.sql.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the TweetDAO interface.
 * 
 * Provides methods to:
 * <ol>
 * <li>Create a new tweet entry.</li>
 * <li>Update an existing tweet entry.</li>
 * <li>Delete an existing tweet entry.</li>
 * <li>Retrieve all tweet entries.</li>
 * <li>Retrieve a single tweet entry via its ID.</li>
 * </ol>
 * 
 * @author Eira Garrett
 */
public class TweetDAOImpl implements TweetDAO {
    private final static Logger LOG = LoggerFactory.getLogger(TweetDAOImpl.class);
    
    private final String URL;
    private final String USER;
    private final String PASSWORD;
    
    /**
     * Initializes a TweetDAOImpl object. Retrieves db URL and
     * credentials from a file in src/main/resources called dbcredentials.properties
     * Should contain:
     * <ul>
     * <li>The connection URL with key url</li>
     * <li>The user name with key user</li>
     * <li>The user's password with key password</li>
     * </ul>
     * @throws IOException 
     */
    public TweetDAOImpl() throws IOException {
        try(InputStream input = new FileInputStream("src/main/resources/dbcredentials.properties");) {
            Properties props = new Properties();
            props.load(input);
            
            this.URL = props.getProperty("url");
            this.USER = props.getProperty("user");
            this.PASSWORD = props.getProperty("password");
        }
    }
    
    /**
     * Creates a new record to store a tweet in the database.
     * Takes as input a StoredTweetContents object.
     * @param tweet           A StoredTweetContents object.
     * @return                An integer representing the number of rows affected.
     * @throws SQLException
     */
    @Override
    public int saveTweet(StoredTweetContents tweet) throws SQLException {
        int result = -1;
        String createQuery = "INSERT INTO tweets(statusID, name, handle, text, numrts, numfvs) VALUES (?, ?, ?, ?, ?, ?)";
        //TODO -- implement user photo storage
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(createQuery);) {
            
            fillTweetStatement(ps, tweet);
            result = ps.executeUpdate();
        }
        LOG.info("Inserted " + result + " new tweet records.");
        return result;
    }
    
    /**
     * Finds all tweets in the current tweet table.
     * @return                  An ArrayList of StoredTweetContent objects.
     * @throws SQLException 
     */
    @Override
    public List<StoredTweetContents> findAll() throws SQLException {
        List<StoredTweetContents> rows = new ArrayList<>();
        
        String selectQuery = "SELECT statusID, name, handle, text, numrts, numfvs FROM tweets";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(selectQuery);
                ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                rows.add(createTweetContents(rs));
            }
        }
        
        LOG.info("Retrieval of all tweets returned " + rows.size() + "records.");
        return rows;
    }
    
    @Override
    public StoredTweetContents findByID(long id) throws SQLException {
        StoredTweetContents tweet = new StoredTweetContents();
        
        String selectQuery = "SELECT statusID, name, handle, text, numrts, numfvs FROM tweets WHERE statusID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(selectQuery);) {
            ps.setLong(1, id);
            
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    tweet = createTweetContents(rs);
                }
            }
        }
        
        LOG.info("Found " + id + "?: " + (tweet != null));
        return tweet;
    }
    
    /**
     * Updates a given tweet record.
     * @param tweet          The tweet to update.
     * @return               An integer representing the number of rows affected.
     * @throws SQLException 
     */
    @Override
    public int updateSavedTweet(StoredTweetContents tweet) throws SQLException {
        int result = -1;
        
        String updateQuery = "UPDATE tweets SET statusID=?, name=?, handle=?, text=?, numrts=?, numfvs=? WHERE statusID=?";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            fillTweetStatement(ps, tweet);
            ps.setLong(7, tweet.getStatusID());
            
            result = ps.executeUpdate();
        }
        
        LOG.info("Updated " + result + "records.");
        return result;
    }
    
    /**
     * Deletes a given tweet record.
     * @param id           A long representing the statusID of the tweet.
     * @return             An integer representing the number of rows affected.
     * @throws SQLException 
     */
    @Override
    public int deleteSavedTweet(long id) throws SQLException {
        int result = -1;
        
        String deleteQuery = "DELETE FROM tweets WHERE statusID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setLong(1, id);
            result = ps.executeUpdate();
        }
        
        LOG.info("Deleted " + result + "records.");
        return result;
    }
    
    // PRIVATE HELPERS //
    
    /**
     * Fills all fields of a prepared statement with the
     * relevant values stored in a SavedTweetContents object.
     * @param ps              The prepared statement to which to bind parameters.
     * @param tweet           The StoredTweetContents object containing the parameters to bind.
     * @throws SQLException 
     */
    private void fillTweetStatement(PreparedStatement ps, StoredTweetContents tweet) throws SQLException {
        ps.setLong(1, tweet.getStatusID());
        ps.setString(2, tweet.getName());
        ps.setString(3, tweet.getHandle());
        ps.setString(4, tweet.getText());
        ps.setInt(5, tweet.getRetweetCount());
        ps.setInt(6, tweet.getFavoriteCount());
    }
    
    /**
     * Extracts information from a resultSet and stores in a SavedTweetContents object.
     * @param resultSet                The resultSet to process.
     * @return                         A StoredTweetContents object containing the relevant data.
     * @throws SQLException 
     */
    private StoredTweetContents createTweetContents(ResultSet resultSet) throws SQLException {
        StoredTweetContents tweet = new StoredTweetContents();
        
        tweet.setStatusID(resultSet.getLong("statusID"));
        tweet.setName(resultSet.getString("name"));
        tweet.setHandle(resultSet.getString("handle"));
        tweet.setText(resultSet.getString("text"));
        tweet.setRetweetCount(resultSet.getInt("numrts"));
        tweet.setFavoriteCount(resultSet.getInt("numfvs"));
        
        return tweet;
    }
}
