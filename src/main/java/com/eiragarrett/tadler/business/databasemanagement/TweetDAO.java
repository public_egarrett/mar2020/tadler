package com.eiragarrett.tadler.business.databasemanagement;

import com.eiragarrett.tadler.business.tweetmanagement.StoredTweetContents;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods.
 * @author Eira Garrett
 */
public interface TweetDAO {
    //Create
    public int saveTweet(StoredTweetContents tweet) throws SQLException;
    
    //Read
    public List<StoredTweetContents> findAll() throws SQLException;
    public StoredTweetContents findByID(long id) throws SQLException;
    
    //Update
    public int updateSavedTweet(StoredTweetContents tweet) throws SQLException;
    
    //Delete
    public int deleteSavedTweet(long id) throws SQLException;
}
