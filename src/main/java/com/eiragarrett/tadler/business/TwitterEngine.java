package com.eiragarrett.tadler.business;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.Status;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Relationship;
import twitter4j.User;

/**
 * A class with methods designed for interaction with the Twitter4J library, comprising such actions as:
 * - Fetching lists of tweets matching various criteria
 * - Posting tweets
 * - Sending and fetching direct messages
 * 
 * @author Eira Garrett
 */
public class TwitterEngine {
    private final static Logger LOG = LoggerFactory.getLogger(TwitterEngine.class);
    private long currentUser;
    
    /**
     * Fetches a handle to the Twitter instance.
     * @return 
     */
    public Twitter getTwitterInstance() {
        return TwitterFactory.getSingleton();
    }
    
    /**
     * Handles the generation of a list of tweets visible on the current user's home timeline. Based on code by Ken Fogel at https://gitlab.com/omniprof/tweetdemo02
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<Status> getTimeline(int page) throws TwitterException {
        LOG.debug("Reached getTimeline.");
        
        Twitter twitter = getTwitterInstance();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        List<Status> statuses = twitter.getHomeTimeline(paging);
        
        return statuses;
    }
    
    /**
     * Handles the generation of a paged list of tweets matching a given search parameter.
     * @param queryString
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<Status> searchTweets(String queryString, int page) throws TwitterException {
        LOG.debug("Reached searchTweets.");
        
        Twitter twitter = getTwitterInstance();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        Query query = new Query(queryString);
        QueryResult result = twitter.search(query);
        
        List<Status> resultTweets = result.getTweets();
        
        return resultTweets;
    }
    
    /**
     * Gets a list of the user's tweets which have recently been retweeted by others.
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<Status> getRetweetsOfMe(int page) throws TwitterException {
        LOG.debug("Reached getRetweetsOfMe.");
        
        Twitter twitter = getTwitterInstance();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        return twitter.getRetweetsOfMe(paging);
    }
    
    /**
     * Gets a list of tweets which have been retweeted by the current user.
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<Status> getMyRetweets(int page) throws TwitterException {
        LOG.debug("Reached getMyRetweets");
        
        Twitter twitter = getTwitterInstance();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        List<Status> allTweets = twitter.getUserTimeline(paging);
        
        return onlyRetweets(allTweets);
        
    }
    
    /**
     * Takes a list of all tweets on the user's timeline and returns only those which are retweets.
     * @param allTweets
     * @return 
     */
    private List<Status> onlyRetweets(List<Status> allTweets) {
        List<Status> output = new ArrayList<>();
        
        if (allTweets != null) {
            allTweets.stream().filter((status) -> (status.isRetweetedByMe())).forEachOrdered((status) -> {
                output.add(status);
            });
        }
        
        return output;
    }
    
    /**
     * Handles the generation of a paged list containing tweets in which the current user was mentioned.
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<Status> getMentions(int page) throws TwitterException {
        LOG.debug("Reached getMentions.");
        
        Twitter twitter = getTwitterInstance();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        List<Status> mentions = twitter.getMentionsTimeline(paging);
        
        return mentions;
    }
    
    /**
     * Retrieves the list of all direct messages within a given conversation.
     * MAY RETURN NULL.
     * 
     * @param page
     * @param userid
     * @return
     * @throws TwitterException 
     */
    public List<DirectMessage> getDMsForConversation(int page, long userid) throws TwitterException {
        LOG.debug("reached getDMs");
        
        Twitter twitter = getTwitterInstance();
        
        currentUser = twitter.getId();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        DirectMessageList allDMs = twitter.getDirectMessages(50);
        
        List<DirectMessage> byUser = trimToUserID(userid, allDMs);
        
        return byUser;
    }
    
    /**
     * Trims the list of the current user's 50 most recent direct messages to those for which the sender or recipient ID matches a given user ID.
     * MAY RETURN NULL.
     * 
     * @param userid
     * @param allDMs
     * @return 
     */
    private List<DirectMessage> trimToUserID(long userid, DirectMessageList allDMs) {
        List<DirectMessage> dmById = new ArrayList<>();
        
        if (!allDMs.isEmpty()) {
            for (DirectMessage dm : allDMs) {
                if (dm.getSenderId() == userid || dm.getRecipientId() == userid) {
                    dmById.add(dm);
                }
            }
        }
        
        return dmById;
    }
    
    /**
     * Returns a list of direct messages -- only one direct message per user, may be used to retrieve unique conversations.
     * @param page
     * @return
     * @throws TwitterException 
     */
    public List<DirectMessage> getConversationList(int page) throws TwitterException {
        LOG.debug("Reached getConversationList.");
        
        Twitter twitter = getTwitterInstance();
        
        currentUser = twitter.getId();
        
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        
        DirectMessageList allDMs = twitter.getDirectMessages(50);
        
        List<DirectMessage> uniqueUsers = getUniqueConversations(allDMs);
        
        return uniqueUsers;
    }
    
    
    /**
     * Returns a list of unique users with whom the current user is exchanging direct messages.
     * @param allDMs
     * @return 
     */
    private List<DirectMessage> getUniqueConversations(DirectMessageList allDMs) {
        List<DirectMessage> allConvos = new ArrayList<>();
        
        ArrayList<Long> ids = new ArrayList<>();
        
        if (!allDMs.isEmpty()) {
            for (DirectMessage dm : allDMs) {
                //Ensure current conversation has not already been added to the list
                if (!(ids.contains(dm.getSenderId())) && !(ids.contains(dm.getRecipientId()))) {
                    //Add the other user's ID to the list to check against
                    if (dm.getSenderId() != currentUser) {
                        ids.add(dm.getSenderId());
                    } else if (dm.getRecipientId() != currentUser) {
                        ids.add(dm.getRecipientId());
                    }
                    
                    allConvos.add(dm);
                }
            }
        }
        
        return allConvos;
    }
    
    /**
     * Sends a tweet. Based on code by Ken Fogel at https://gitlab.com/omniprof/tweetdemo02
     * @param tweet
     * @throws TwitterException 
     */
    public void createTweet(String tweet) throws TwitterException {
        LOG.debug("Create tweet: ", tweet);
        
        Twitter twitter = getTwitterInstance();
        Status status = twitter.updateStatus(tweet);
    }
    
    /**
     * Sends a direct message to the given user based on that user's handle.
     * @param handle
     * @param message
     * @throws TwitterException 
     */
    public void sendDirectMessage(String handle, String message) throws TwitterException {
        LOG.debug("Send Direct Message by handle: ", handle, message);
        
        Twitter twitter = getTwitterInstance();
        twitter.sendDirectMessage(handle, message);
    }
    
    /**
     * Sends a direct message to the user based on that user's ID.
     * @param id
     * @param message
     * @throws TwitterException 
     */
    public void sendDirectMessage(long id, String message) throws TwitterException {
        LOG.debug("Send Direct Message by id: ", id, message);
        
        Twitter twitter = getTwitterInstance();
        twitter.sendDirectMessage(id, message);
    }
    
    /**
     * Returns the ID of the currently authenticated user.
     * @return
     * @throws TwitterException 
     */
    public long getCurrentUserID() throws TwitterException {
        LOG.debug("Reached getCurrentUserID.");
        
        Twitter twitter = getTwitterInstance();
        
        return twitter.getId();
    }
    
    /**
     * Attempts to like a given status, identified by status id.
     * @param id
     * @return
     * @throws TwitterException 
     */
    public boolean addLike(long id) throws TwitterException {
        LOG.debug("Reached addLike.");
        
        Twitter twitter = getTwitterInstance();
        
        Status favorited = twitter.createFavorite(id);
        
        return favorited != null;
    }
    
    /**
     * Creates a retweet of a given status without comment.
     * @param id
     * @return
     * @throws TwitterException 
     */
    public boolean retweetStatusNoComment(long id) throws TwitterException {
        LOG.debug("Reached retweetStatus.");
        
        Twitter twitter = getTwitterInstance();
        
        Status retweeted = twitter.retweetStatus(id);
        
        return retweeted != null;
    }
    
    /**
     * Checks to see whether or not the current user is following the author of a given status.
     * @param user
     * @return
     * @throws TwitterException 
     */
    public boolean isFollowingAuthor(User user) throws TwitterException {
        LOG.debug("Reached isFollowingAuthor.");
        
        Twitter twitter = getTwitterInstance();
        
        Relationship relationship = twitter.showFriendship(twitter.getId(), user.getId());
        
        return relationship.isTargetFollowedBySource();
    }
    
    /**
     * Checks to see whether or not the current user is followed by another.
     * @param user
     * @return
     * @throws TwitterException 
     */
    public boolean isFollowedByAuthor(User user) throws TwitterException {
        LOG.debug("Reached isFollowedByAuthor");
            
        Twitter twitter = getTwitterInstance();
        
        Relationship relationship = twitter.showFriendship(twitter.getId(), user.getId());
        
        return relationship.isTargetFollowingSource();
    }
    
    /**
     * Attempts to follow the author of a given tweet.
     * @param userID
     * @return
     * @throws TwitterException 
     */
    public boolean followAuthor(long userID) throws TwitterException {
        LOG.debug("Reached followAuthor.");
        
        Twitter twitter = getTwitterInstance();
       
        
        User followed = twitter.createFriendship(userID);
        
        return true;
    }
    
    /**
     * Attempts to unfollow the author of a given tweet.
     * @param userID
     * @return
     * @throws TwitterException 
     */
    public boolean unfollowAuthor(long userID) throws TwitterException {
        LOG.debug("Reached unfollowAuthor.");
        
        Twitter twitter = getTwitterInstance();
        
        User unfollowed = twitter.destroyFriendship(userID);
        
        return true;
    }
    
    /**
     * Returns a User object for the author of a given tweet based on that tweet's status ID.
     * @param statusID
     * @return
     * @throws TwitterException 
     */
    public User getAuthor(long statusID) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        
        Status status = twitter.showStatus(statusID);
        
        return status.getUser();
    }
    
    /**
     * Generates a URL string for the given status.
     * @param statusID
     * @return
     * @throws TwitterException 
     */
    public String getURLString(long statusID) throws TwitterException {
        LOG.debug("reached getURLString");
        
        Twitter twitter = getTwitterInstance();
        
        Status status = twitter.showStatus(statusID);
        
        String url = "https://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId();
        
        return url;
    }
}
