package com.eiragarrett.tadler.business;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Eira Garrett
 */
public class PropertiesBean {
    private final StringProperty consumerKey;
    private final StringProperty consumerSecret;
    private final StringProperty accessToken;
    private final StringProperty accessTokenSecret;
    
    public PropertiesBean() {
        super();
        
        this.consumerKey = new SimpleStringProperty("");
        this.consumerSecret = new SimpleStringProperty("");
        this.accessToken = new SimpleStringProperty("");
        this.accessTokenSecret = new SimpleStringProperty("");
    }
    
    //===========================================
    
    public void setConsumerKey(String consumerKey) {
        this.consumerKey.set(consumerKey);
    }
    
    public String getConsumerKey() {
        return this.consumerKey.get();
    }
    
    public StringProperty getConsumerKeyProperty() {
        return this.consumerKey;
    }
    
    //=====================================================
    
    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret.set(consumerSecret);
    }
    
    public String getConsumerSecret() {
        return this.consumerSecret.get();
    }
    
    public StringProperty getConsumerSecretProperty() {
        return this.consumerSecret;
    }
    
    //=============================================
    
    public void setAccessToken(String accessToken) {
        this.accessToken.set(accessToken);
    }
    
    public String getAccessToken() {
        return this.accessToken.get();
    }
    
    public StringProperty getAccessTokenProperty() {
        return this.accessToken;
    }
    
    
    //===============================================
    
    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret.set(accessTokenSecret);
    }
    
    public String getAccessTokenSecret() {
        return this.accessTokenSecret.get();
    }
    
    public StringProperty getAccessTokenSecretProperty() {
        return this.accessTokenSecret;
    }
}
