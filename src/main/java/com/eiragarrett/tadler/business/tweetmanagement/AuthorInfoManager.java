package com.eiragarrett.tadler.business.tweetmanagement;

import com.eiragarrett.tadler.business.TwitterEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Manages the retrieval of information related to a single user object representing the author of a given tweet.
 * @author Eira Garrett
 */
public class AuthorInfoManager {
    private final static Logger LOG = LoggerFactory.getLogger(AuthorInfoManager.class);
    
    private final User user;
    private TwitterEngine engine;
    
    /**
     * Instantiates an AuthorInfoManager object based on the provided status ID.
     * @param statusID
     * @throws TwitterException 
     */
    public AuthorInfoManager(long statusID) throws TwitterException {
        this.engine = new TwitterEngine();
        this.user = engine.getAuthor(statusID);
    }
    
    /**
     * Returns the name of the user.
     * @return 
     */
    public String getName() {
        return user.getName();
    }
    
    /**
     * Returns the Twitter handle of the user.
     * @return 
     */
    public String getHandle() {
        return user.getScreenName();
    }
    
    /**
     * Returns the ID of the user.
     * @return 
     */
    public long getID() {
        return user.getId();
    }
    
    /**
     * Returns the follower count of the user.
     * @return 
     */
    public int getFollowerCount() {
        return user.getFollowersCount();
    }
    
    /**
     * Returns the following count of the user.
     * @return 
     */
    public int getFollowingCount() {
        return user.getFriendsCount();
    }
    
    /**
     * Returns the user's profile description.
     * @return 
     */
    public String getProfileInfo() {
        return user.getDescription();
    }
            
    /**
     * Returns a boolean indicating whether or not the currently authenticated user is following the author of the tweet.
     * @return
     * @throws TwitterException 
     */
    public Boolean isFollowedByMe() throws TwitterException {
        return engine.isFollowingAuthor(user);
    }
    
    /**
     * Returns whether or not the currently authenticated user is followed by the author of the tweet.
     * @return
     * @throws TwitterException 
     */
    public Boolean isFollowingMe() throws TwitterException {
        return engine.isFollowedByAuthor(user);
    }
}
