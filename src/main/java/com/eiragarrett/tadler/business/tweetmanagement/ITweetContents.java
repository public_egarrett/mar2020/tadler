package com.eiragarrett.tadler.business.tweetmanagement;

import javafx.scene.image.Image;

/**
 * An interface for classes that store information related to individual tweets.
 * @author Eira Garrett
 */
public interface ITweetContents {
    
    public String getName();
    
    public String getText();
    
    //get profile image -- refactor in TweetContents
    
    public String getHandle();
    
    public int getRetweetCount();
    
    public int getFavoriteCount();
    
    public Image getProfileImage();
    
    public long getStatusID();
}
