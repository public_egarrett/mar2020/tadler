package com.eiragarrett.tadler.business.tweetmanagement;

import java.util.Objects;
import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages information related to a single Tweet for storing in a database.
 * @author Eira Garrett
 */
public class StoredTweetContents implements ITweetContents {
    private final static Logger LOG = LoggerFactory.getLogger(StoredTweetContents.class);
    private String name;
    private String handle;
    private String text;
    private int retweets;
    private int favorites;
    private Image profileImg;
    private long statusID;
    
    public StoredTweetContents(String name, String handle, String text, int retweets, int favorites, final long statusID) {
        this.name = name;
        this.handle = handle;
        this.text = text;
        this.retweets = retweets;
        this.favorites = favorites;
        this.statusID = statusID;
    }
    
    public StoredTweetContents() {
        this("", "", "", -1, -1, -1);
    }
    
///////////// Getters //////////////
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public String getText() {
        return this.text;
    }
    
    @Override
    public String getHandle() {
        return this.handle;
    }
    
    @Override
    public int getRetweetCount() {
        return this.retweets;
    }
    
    @Override
    public int getFavoriteCount() {
        return this.favorites;
    }
    
    @Override
    public long getStatusID() {
        return this.statusID;
    }
    
    @Override
    public Image getProfileImage() {
        return this.profileImg;
    }
    
    //////////// Setters ///////////
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setHandle(final String handle) {
        this.handle = handle;
    }
    
    public void setText(final String text) {
        this.text = text;
    }
    
    public void setRetweetCount(final int count) {
        this.retweets = count;
    }
    
    public void setFavoriteCount(final int count) {
        this.favorites = count;
    }
    
    public void setStatusID(final long id) {
        this.statusID = id;
    }
    
    public void setProfileImage(Image image) {
        this.profileImg = image;
    }
    
    /**
     * Override equals method to ensure tweet has not already been stored.
     * @param obj
     * @return
     */
    @Override
    public boolean equals (Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        StoredTweetContents other = (StoredTweetContents)obj;
        
        if(statusID != other.getStatusID()) {
            return false;
        }
        
        if(name == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!name.equals(other.getName())) {
            return false;
        }
        
        if (handle == null) {
            if (other.getHandle() != null) {
                return false;
            }
        } else if (!handle.equals(other.getHandle())) {
            return false;
        }
        
        if (text == null) {
            if (other.getText() != null) {
                return false;
            }
        } else if (!text.equals(other.getText())) {
            return false;
        }
        
        if (retweets != other.getRetweetCount()) {
            return false;
        }
        
        if (favorites != other.getFavoriteCount()) {
            return false;
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.handle);
        hash = 29 * hash + Objects.hashCode(this.text);
        hash = 29 * hash + this.retweets;
        hash = 29 * hash + this.favorites;
        hash = 29 * hash + Objects.hashCode(this.statusID);
        return hash;
    }
}
