package com.eiragarrett.tadler.business.tweetmanagement;

import com.eiragarrett.tadler.controller.TimelineFXMLController;
import com.eiragarrett.tadler.controller.TweetCellFXMLController;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;

/**
 * Generates the contents of a list cell for a given timeline.
 * @author Eira Garrett
 */
public class TweetContentsCell extends ListCell<ITweetContents>{
    private final static Logger LOG = LoggerFactory.getLogger(TweetContentsCell.class);
    TimelineFXMLController controller;
    
    public TweetContentsCell() {
        
    }
    
    public TweetContentsCell(TimelineFXMLController controller) {
        this.controller = controller;
    }
    
    /**
     * 
     * @param item
     * @param empty 
     */
    @Override
    protected void updateItem(ITweetContents item, boolean empty) {
        super.updateItem(item, empty);
        
        LOG.debug("Reached TweetContentsCell.updateItem.");
        
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            try {
                setGraphic(getTweetContentsCell(item));
            } catch (IOException e) {
                LOG.error("Could not load cell: ", e);
            }
        }
    }
    
    
    /**
     * Generates a node designed to display the contents of a single tweet. Based on code by Ken Fogel at https://gitlab.com/omniprof/tweetdemo02
     * @param contents
     * @return 
     */
    private Node getTweetContentsCell(ITweetContents contents) throws IOException {
        HBox node = new HBox();
        node.setSpacing(10);
        
        //Check whether or not this is a saved tweet
        boolean isSaved = contents instanceof StoredTweetContents;
        
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/TweetCellFXML.fxml"), bundle);
            node.getChildren().add(loader.load());
            TweetCellFXMLController cellController = loader.getController();
            cellController.setParentController(controller);
            
            cellController.populateCell(contents, isSaved);  
        } catch (IOException e) {
            LOG.error("Could not load Tweet Cell FXML: " + e);
        }
        
        return node;
    }
}
