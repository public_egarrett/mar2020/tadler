package com.eiragarrett.tadler.business.tweetmanagement;

import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * Stores all necessary contents of a given Tweet. Comprises:
 * - The name of the user who authored the tweet
 * - The Twitter handle of the user who authored the tweet
 * - The profile image of the user who authored the tweet
 * - The text contents of the tweet
 * - The retweet count of the tweet
 * - The like/favourite count of the tweet
 * 
 * @author Eira Garrett
 */
public class TweetContents implements ITweetContents {
    private final static Logger LOG = LoggerFactory.getLogger(TweetContents.class);
    
    private final Status status;
    
    public TweetContents(Status status) {
        this.status = status;
    }
    
    @Override
    public String getName() {
        return status.getUser().getName();
    }
    
    @Override
    public String getText() {
        return status.getText();
    }
    
    @Override
    public Image getProfileImage() {
        Image profileImage = new Image(status.getUser().getProfileImageURL(), 48, 48, false, true);
        
        return profileImage;
    }
    
    @Override
    public String getHandle() {
        return status.getUser().getScreenName();
    }
    
    @Override
    public int getRetweetCount() {
        return status.getRetweetCount();
    }
    
    @Override
    public int getFavoriteCount() {
        return status.getFavoriteCount();
    }
    
    @Override
    public long getStatusID() {
        return status.getId();
    }
}
