package com.eiragarrett.tadler.business.tweetmanagement;

import com.eiragarrett.tadler.business.TwitterEngine;
import com.eiragarrett.tadler.business.databasemanagement.TweetDAO;
import com.eiragarrett.tadler.business.databasemanagement.TweetDAOImpl;
import java.io.IOException;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import java.util.List;
import javafx.collections.ObservableList;
import twitter4j.TwitterException;

/**
 * A class to manage the population of lists representing Twitter timelines matching various criteria:
 * - Tweets in the current user's home timeline
 * - Tweets matching given search parameters
 * - Tweets in which the current user was mentioned
 * 
 * @author Eira Garrett
 */
public class TimelineManager {
    private final static Logger LOG = LoggerFactory.getLogger(TimelineManager.class);
    
    private final ObservableList<ITweetContents> tweets;
    
    private final TwitterEngine twitterEngine;
    
    private int page;
    
    public TimelineManager(ObservableList<ITweetContents> tweets) {
        twitterEngine = new TwitterEngine();
        
        this.tweets = tweets;
        
        page = 1;
    }
    
    /**
     * Fills a timeline with tweets from the current user's home timeline.
     * @throws TwitterException 
     */
    public void fillTimeline() throws TwitterException {
        List<Status> homeTimeline = twitterEngine.getTimeline(page);
        
        homeTimeline.forEach((status) -> {
            tweets.add(tweets.size(), new TweetContents(status));
        });
        
        page++;
    }
    
    /**
     * Fills a timeline with tweets matching a given search query.
     * @param query
     * @throws TwitterException 
     */
    public void fillSearchResults(String query) throws TwitterException {
        List<Status> searchResults = twitterEngine.searchTweets(query, page);
        
        searchResults.forEach((status) -> {
            tweets.add(tweets.size(), new TweetContents(status));
        });
        
        page++;
    }
    
    /**
     * Fills a timeline with only tweets in which the current user was mentioned.
     * @throws TwitterException 
     */
    public void fillMentions() throws TwitterException {
        List<Status> mentions = twitterEngine.getMentions(page);
        
        mentions.forEach((status) -> {
            tweets.add(tweets.size(), new TweetContents(status));
        });
        
        page++;
    }
    
    /**
     * Fills a list of tweets retweeted by the current user.
     * @throws TwitterException 
     */
    public void fillRetweetsByMe() throws TwitterException {
        List<Status> myRetweets = twitterEngine.getMyRetweets(page);
        
        myRetweets.forEach((status) -> {
            tweets.add(tweets.size(), new TweetContents(status));
        });
        
        page++;
    }
    
    /**
     * Fills a list of tweets stored in the database.
     * @throws SQLException
     * @throws IOException
     */
    public void fillSavedTweets() throws SQLException, IOException {
        TweetDAO dao = new TweetDAOImpl();
        
        List<StoredTweetContents> storedTweets = dao.findAll();
        
        storedTweets.forEach((tweet) -> {
            tweets.add(tweets.size(), tweet);
        });
    }
    
    /**
     * Fills a list of tweets made by the current user and retweeted by others.
     * @throws TwitterException 
     */
    public void fillRetweetsByOthers() throws TwitterException {
        List<Status> theirRetweets = twitterEngine.getRetweetsOfMe(page);
        
        theirRetweets.forEach((status) -> {
            tweets.add(tweets.size(), new TweetContents(status));
        });
        
        page++;
    }
    
    /**
     * Resets the page.
     */
    public void resetPage() {
        tweets.clear();
        page = 1;
    }
}
