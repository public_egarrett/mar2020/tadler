package com.eiragarrett.tadler.business.directmessagemanagement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import twitter4j.TwitterException;

/**
 * Generates the list cells for the list of DM conversations.
 * @author Eira Garrett
 */
public class DMConversationListCell extends ListCell<DMContents> {
    private final static Logger LOG = LoggerFactory.getLogger(DMConversationListCell.class);
    
    @Override
    protected void updateItem(DMContents item, boolean empty) {
        super.updateItem(item, empty);
        
        LOG.debug("Reached DMConversationListCell.updateItem.");
        
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            try {
                setGraphic(getConversationHeaderCell(item));
            } catch (TwitterException ex) {
                LOG.error("TwitterException thrown in DMConversationListCell.updateItem: ", ex);
                //TODO -- generate and display error message cell
            }
        }
    }
    
    /**
     * Generates a node designed to display the user image and user name of a single user with which the currently signed-in user
     * has an existing DM conversation.
     * 
     * @param contents
     * @return 
     */
    private Node getConversationHeaderCell(DMContents contents) throws TwitterException {
        VBox node = new VBox();
        node.setSpacing(10);
        
        Image image = new Image(contents.getSenderProfileImageURL());
        ImageView imageView = new ImageView(image);
        
        Text name = new Text("@" + contents.getSendingUserHandle() + " to @" + contents.getRecipientUserHandle());
        name.setWrappingWidth(100);
        
        node.getChildren().addAll(imageView, name);
        
        return node;
    }
}
