package com.eiragarrett.tadler.business.directmessagemanagement;

import com.eiragarrett.tadler.business.TwitterEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import javafx.collections.ObservableList;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;

/**
 * A class for managing the display of direct message conversations + single direct message conversations.
 * @author Eira Garrett
 */
public class DMManager {
    private final static Logger LOG = LoggerFactory.getLogger(DMManager.class);
    
    private final ObservableList<DMContents> dms;
    
    private final TwitterEngine twitterEngine;
    
    private int page;
    
    public DMManager(ObservableList<DMContents> dms) {
        twitterEngine = new TwitterEngine();
        
        this.dms = dms;
        
        page = 1;
    }
    
    /**
     * Fills out a list of direct message conversations in which the current user is taking part.
     * @throws TwitterException 
     */
    public void fillConversationList() throws TwitterException {
        List<DirectMessage> convos = twitterEngine.getConversationList(page);
        
        convos.forEach((dm) -> {
            dms.add(dms.size(), new DMContents(dm));
        });
        
        page++;
    }
    
    /**
     * Fills out a list of messages within a single direct message conversation.
     * @param userid
     * @throws TwitterException 
     */
    public void fillSingleConvo(long userid) throws TwitterException {
        List<DirectMessage> convo = twitterEngine.getDMsForConversation(page, userid);
        
        convo.forEach((dm) -> { 
            dms.add(dms.size(), new DMContents(dm));
        });
        
        page++;
    }
    
    public void resetPage() {
        this.page = 1;
        this.dms.clear();
    }
}
