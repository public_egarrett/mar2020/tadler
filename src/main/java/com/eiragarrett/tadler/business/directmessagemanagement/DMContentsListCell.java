package com.eiragarrett.tadler.business.directmessagemanagement;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Generates the list cells for DM conversations.
 * @author Eira Garrett
 */
public class DMContentsListCell extends ListCell<DMContents> {
    private final static Logger LOG = LoggerFactory.getLogger(DMContentsListCell.class);
    
    @Override
    protected void updateItem(DMContents item, boolean empty) {
        super.updateItem(item, empty);
        
        LOG.info("Reached DMContentsCellList.updateItem.");
        
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            try {
                setGraphic(getDMContentsCell(item));
            } catch (TwitterException e) {
                LOG.error("Unable to populate conversation cell: ", e);
                //TODO -- generate error message cell
            }
        }
    }
    
    /**
     * Generates a node designed to display the contents of a single direct message. Based on code by Ken Fogel at https://gitlab.com/omniprof/tweetdemo02
     * @param contents
     * @return 
     */
    private Node getDMContentsCell(DMContents contents) throws TwitterException {
        HBox node = new HBox();
        node.setSpacing(10);
        
        Image image = new Image(contents.getSenderProfileImageURL(), 48, 48, true, false);
        ImageView imageView = new ImageView(image);
        
        Text name = new Text("@" + contents.getSendingUserHandle());
        name.setWrappingWidth(450);
        
        Text text = new Text(contents.getContents());
        text.setWrappingWidth(450);
        
        VBox vbox = new VBox();
        vbox.getChildren().addAll(name, text);
        node.getChildren().addAll(imageView, vbox);
        
        return node;
    }
    
}
