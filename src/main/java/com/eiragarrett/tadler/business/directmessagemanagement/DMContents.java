package com.eiragarrett.tadler.business.directmessagemanagement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Stores all necessary contents of a given direct message. Comprises:
 * - The ID of the sender
 * - The ID of the recipient
 * - The display name of the sender
 * - The contents of the message
 * - The sending user's profile image
 * 
 * @author Eira Garrett
 */
public class DMContents {
    private final static Logger LOG = LoggerFactory.getLogger(DMContents.class);
    
    private final DirectMessage message;
    private final Twitter twitter;
    
    public DMContents(DirectMessage message) {
        this.message = message;
        this.twitter = TwitterFactory.getSingleton();
    }
    
    /**
     * Returns the ID of the sender.
     * @return 
     */
    public long getSendingUser() {
        return message.getSenderId();
    }
    
    /**
     * Returns the ID of the recipient.
     * @return 
     */
    public long getReceivingUser() {
        return message.getRecipientId();
    }
    
    /**
     * Returns the handle of the sending user.
     * @return
     * @throws TwitterException 
     */
    public String getSendingUserHandle() throws TwitterException {
        long id = message.getSenderId();
        
        ResponseList<User> user = twitter.lookupUsers(id);
        
        return user.get(0).getScreenName();
    }
    
    /**
     * Returns the handle of the recipient of the message.
     * @return
     * @throws TwitterException 
     */
    public String getRecipientUserHandle() throws TwitterException {
        long id = message.getRecipientId();
        
        ResponseList<User> user = twitter.lookupUsers(id);
        
        return user.get(0).getScreenName();
    }
    
    /**
     * Returns the contents of the direct message.
     * @return 
     */
    public String getContents() {
        return message.getText();
    }
    
    /**
     * Returns the URL of the profile image of the user who sent the message.
     * @return
     * @throws TwitterException 
     */
    public String getSenderProfileImageURL() throws TwitterException {
        long id = message.getSenderId();
        
        ResponseList<User> user = twitter.lookupUsers(id);
        
        return user.get(0).getProfileImageURL();
    }
}
