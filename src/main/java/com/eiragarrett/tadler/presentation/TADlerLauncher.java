/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eiragarrett.tadler.presentation;

import com.eiragarrett.tadler.controller.LaunchFXMLController;
import com.eiragarrett.tadler.controller.MainFXMLController;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

/**
 * Handles the initial launch and application flow of TADler, a Twitter At Dawson Twitter client.
 * @author Eira Garrett
 * @version 2019/10/06
 */
public class TADlerLauncher extends Application {
    private final static Logger LOG = LoggerFactory.getLogger(TADlerLauncher.class);
    
    private Stage primaryStage;
    
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * Displays one of two initial application states depending on whether or not a properties file exists.
     * If it does, main application is displayed. If not, a form is displayed requesting relevant information.
     * @param primaryStage
     * @throws Exception 
     */
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setResizable(false);
        
        //Check to see if properties file exists
        File propFile = new File("src/main/resources/twitter4j.properties");
        
        //If properties file exists, load and proceed directly to main
        if (propFile.exists() && !(propFile.isDirectory())) {
            LOG.debug("Properties file found.");
            
            //TODO - Instantiate Twitter object, etc
            
            //Load main scene
            getMainScene();
        } else {
            //If properties file does not exist, display form
            LOG.debug("Properties file not found.");
            getForm();
        }
    }
    
    /**
     * Handles the initialization and display of the main application scene.
     */
    private void getMainScene() {
        try {
            Locale locale = new Locale("en_CA");
            ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainFXML.fxml"), bundle);
            
            Parent root = loader.load();
            MainFXMLController controller = loader.getController();
            controller.loadTimelineOnLaunch();
            Scene scene = new Scene(root);
            
            
            
            this.primaryStage.setTitle("TADler");
            this.primaryStage.setScene(scene);
            this.primaryStage.setResizable(false);
            this.primaryStage.show();
            
        } catch (IOException | IllegalStateException ex) {
          //Log, alert
          LOG.error("Main scene could not be loaded: ", ex);
        } 
    }
    
    /**
     * Handles the initialization and display of the form used to generate the properties file.
     */
    private void getForm() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LaunchFXML.fxml"));
            Parent root = loader.load();
            LaunchFXMLController launchController = loader.getController();
            Scene scene = new Scene(root);
            
            primaryStage.setTitle("TADler");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException | IllegalStateException ex) {
            //Log, alert
            LOG.error("Main scene could not be loaded: ", ex);
        }
    }
}
