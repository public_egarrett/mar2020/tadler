TADLER is a desktop Twitter client developed using JavaFX and Twitter4J. This application was written as a term project for a Fall 2019 Java development course at Dawson College.

It implements, however imperfectly, the following functionalities:

    * Viewing a user's home timeline.
    * Posting tweets to a user's home timeline.
    * Basic search and result display functionality.
    * Liking and retweeting of individual tweets.
    * Direct messaging to other users.
    * For demonstration purposes, writing data to and retrieving data from a database storing 'favorited'/saved Tweets.

At this time, running the application requires Twitter API keys.